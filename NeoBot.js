const cheerio = require('cheerio');
const Discord = require("discord.js");
const client = new Discord.Client();
const request = require('request');
const fs = require('fs');
const config = require("./config.js");
let Steam = require('machinepack-steam');

let users = [];
let DATA_LOADED = false;
// let CHANNEL_ID = '320317408403390465'; //DEV
// let CHANNEL_ID = '345236731551023113'; //memes
let CHANNEL_ID = '455327349421113344'; //memes - bot channel

let games = [];

let inventories = [];

let sms = [];

let sendMessage = () => {

};

class User {
    constructor(id, name, tract, discordID, xp, isWatching, number, currentGameId, level) {
        this.steamID = id;
        this.xp = xp;
        this.level = level;
        this.name = name;
        this.tract = tract;
        this.discordID = discordID;
        this.isWatching = isWatching;
        this.notificationNumber = number;
        this.currentGameId = currentGameId;
        console.log(this);
    }

    getMovie(c) {
        let avatar = c.users.get(this.discordID).avatarURL;
        let obj = this;
        if(obj.tract === '') return;
        request('https://trakt.tv/users/' + this.tract, function (error, response, body) {
            if (!error) {
                //fs.writeFileSync(`trakt/${obj.tract}-${Date.now()}`, body, 'UTF-8');
                let jq = cheerio.load(body);
                if(jq.html().length < 6000) return;
                let content = jq("#watching-now-wrapper").html();
                if (content) {
                    let watching = cheerio.load(content);
                    //let movies = watching("strong");
                    let movie = "";
                    watching("h3 a").contents().map(function() {
                            movie += watching(this).text().trim() + " ";
                    }).get();
                    movie = movie.replace("  ", " ");
                    let movieURL = watching("h3 a").attr('href');
                    if (obj.isWatching !== movie) {
                        obj.isWatching = movie;
                        console.log(obj.name + " started watching " + movie);
                        client.channels.get(CHANNEL_ID).sendMessage(obj.name + " started watching " + movie + "\n" + "https://trakt.tv" + movieURL);
                    }
                } else {
                    if (obj.isWatching) console.log(obj.name + " is no longer watching");
                    //obj.isWatching = '';
                }
            }
        });
    }
	
	getCurrentPlayingGame(c) {
        return;
		let obj = this;
        if(obj.steamID === '') return;
		request('http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key='+config.steam.api+'&steamids=' + obj.steamID, function (error, response, body) {
		    if(!error) {
                if (body.length > 0) {
                    try {
                        let playerInfo = JSON.parse(body).response.players[0];
                        let currentGameId = playerInfo.gameid;
                        if (currentGameId) {
                            if (obj.currentGameId !== currentGameId) {
                                request('http://store.steampowered.com/api/appdetails?appids=' + currentGameId, function (error2, response2, body2) {
                                    if(!error2) {
                                        if (body2.length > 0) {
                                            try {
                                                let currentGameName = JSON.parse(body2)[currentGameId].data.name;
                                                let message = obj.name + " is playing " + currentGameName + "\n" + "http://store.steampowered.com/app/" + currentGameId;
                                                console.log(message);
                                                c.channels.get(CHANNEL_ID).sendMessage(message);
                                                obj.currentGameId = currentGameId;
                                            } catch (e) {
                                                console.log(e);
                                            }
                                        }
                                    }
                                });
                            }
                        } else {
                            obj.currentGameId = "";
                        }
                    } catch (e) {
                        console.log(e);
                    }
                }
            } else {
		        console.log(error);
            }
		});
	}
	
	getCSGOInventory(c) {
		let obj = this;
        if(obj.steamID === '') return;
		request('http://steamcommunity.com/profiles/'+ obj.steamID +'/inventory/json/730/2', function (error, response, body) {
		    if(!error && response.statusCode === 200) {
                if (body.length > 0) {
                    try {
                        let inventory = JSON.parse(body);
						let itemKeys = Object.keys(inventory.rgInventory);
						let itemsCount = itemKeys.length;
						if (itemsCount != inventories[obj.steamID].item_count) {
							itemKeys.forEach(key => {
								if (inventories[obj.steamID].items.indexOf(key) < 0) {
									let item = inventory.rgInventory[key];
									let itemDescription = inventory.rgDescriptions[`${item.classid}_${item.instanceid}`];
									if (itemDescription) {
										let url = "http://steamcommunity.com/market/listings/730/" + escape(itemDescription.market_name);
										c.channels.get(CHANNEL_ID).sendMessage({
											"embed": {
												"url": url,
												"description": `${obj.name} added new CS:GO item [${itemDescription.market_name}](${url})`
											}
										});
										c.channels.get(CHANNEL_ID).sendMessage({
											"embed": {
												"image": {
													"url": "http://community.edgecast.steamstatic.com/economy/image/" + itemDescription.icon_url,
												}
											}
										});
									}
								}
							});
						}
						inventories[obj.steamID] = {
							item_count: itemsCount,
							items: itemKeys
						};
						fs.writeFileSync("inventories/" + obj.steamID + ".inv", JSON.stringify(inventories[obj.steamID]), 'UTF-8');
                    } catch (e) {
                        console.log(e);
                    }
                }
            } else {
		        console.log(error);
            }
		});
	}

    getXP(c) {
        let obj = this;
        if(obj.steamID === '') return;
        request('http://api.steampowered.com/IPlayerService/GetBadges/v1/?key='+config.steam.api+'&steamid=' + obj.steamID, function (error, response, body) {
            if(!error) {
                try {
                    if (body.length > 0) {
                        let exp;
                        let lvl;
                        try {
                            let response = JSON.parse(body).response;
                            exp = response.player_xp;
                            lvl = response.player_level;
                        } catch (e) {
                            console.log(e);
                            return;
                        }
                        if (obj.level !== lvl) {
                            let message = obj.name + " is now level " + (lvl) + " on Steam!";
                            c.channels.get(CHANNEL_ID).sendMessage(message);
                            console.log(message);
                            obj.level = lvl;
                        }
                        if (obj.xp !== exp) {
                            let message = obj.name + " just gained " + (exp - obj.xp) + "XP on Steam!";
                            c.channels.get(CHANNEL_ID).sendMessage(message);
                            console.log(message);
                            if (!sms[obj.steamID]) sms[obj.steamID] = 0;
                            sms[obj.steamID] += (exp - obj.xp);
                            obj.xp = exp;
                        }
                    }
                } catch (e) {
                    console.log(e);
                }
            } else {
                console.log(error);
            }
        });
    }
    getGames(c) {
        let obj = this;
        if(obj.steamID === '') return;
        Steam.getOwnedGames({
            steamid: obj.steamID,
            key: config.steam.api,
            include_appinfo: 1,
            include_played_free_games: 0
        }).exec({
            error: (err) => {
                console.log(err);
            },
            success: (result) => {
                if(result.game_count > games[obj.steamID].game_count) {
                    //c.channels.get(CHANNEL_ID).sendMessage((result.game_count - games[obj.steamID].game_count) + " new games found on " + obj.name + " steam account");
                    let new_games = [];
                    for(let i = 0; i < result.games.length; i++) {
                        let newGame = result.games[i];
                        let found = false;
                        for(let j = 0; j < games[obj.steamID].games.length; j++) {
                            let oldGame = games[obj.steamID].games[j];
                            if(oldGame.appid === newGame.appid) found = true;
                        }
                        if(!found) {
                            c.channels.get(CHANNEL_ID).sendMessage(obj.name + " added " + newGame.name + "\n" + "http://store.steampowered.com/app/" + newGame.appid);
                        }
                    }
                    games[obj.steamID] = result;
                    fs.writeFileSync("games/" + obj.steamID + ".games", JSON.stringify(result), 'UTF-8');
                }
            }
        });
    }
    sendSMS() {
        let obj = this;
        if(obj.notificationNumber === '') return;
        if(sms[obj.steamID]) {
            if(sms[obj.steamID] >= 100) {
                let message = obj.name + " just gained " + sms[obj.steamID] + "XP on Steam!";
                let url = 'https://www.smsbiuras.lt/send.php?username=' + config.sms.username + '&password=' + config.sms.password + '&message=' + message + '&from=' + config.sms.sender + '&to=' + obj.notificationNumber;
                console.log(url);
                request(url, function (error, response, body) {
                    console.log(body);
                    console.log("SMS sent to: ", obj.notificationNumber);
                });
                sms[obj.steamID] = 0;
            }
        }
    }
}


let openCell = (msg, map, mapVisible, x, y) => {
    if (mapVisible[y][x] === 'n') {
        if (map[y][x] === 'x') {
            console.log("Game Over");
            process.exit(0);
        } else if (map[y][x] === 0) {
            try {
                mapVisible[y][x] = 0;
                let a;
                if (x > 0 && y > 0) a = openCell(msg, map, mapVisible, x - 1, y - 1);
                map = a.map;
                mapVisible = a.mapVisible;
                if (x > 0 && y < map.length - 1) a = openCell(msg, map, mapVisible, x - 1, y + 1);
                map = a.map;
                mapVisible = a.mapVisible;
                if (x < map[y].length - 1 && y > 0) a = openCell(msg, map, mapVisible, x + 1, y - 1);
                map = a.map;
                mapVisible = a.mapVisible;
                if (x < map[y].length - 1 && y < map.length - 1) a = openCell(msg, map, mapVisible, x + 1, y + 1);
                map = a.map;
                mapVisible = a.mapVisible;
                if (x > 0) a = openCell(msg, map, mapVisible, x - 1, y);
                map = a.map;
                mapVisible = a.mapVisible;
                if (x < map[y].length - 1) a = openCell(msg, map, mapVisible, x + 1, y);
                map = a.map;
                mapVisible = a.mapVisible;
                if (y > 0) a = openCell(msg, map, mapVisible, x, y - 1);
                map = a.map;
                mapVisible = a.mapVisible;
                if (y < map.length - 1) a = openCell(msg, map, mapVisible, x, y + 1);
                map = a.map;
                mapVisible = a.mapVisible;
            } catch (e) {
                console.log(e);
            }
        } else {
            mapVisible[y][x] = map[y][x];
        }
    } else {
        console.log("Cannot open cell at " + x + " " + y);
    }
    return {map: map, mapVisible: mapVisible};
};

try {
    let data = fs.readFileSync('data.dat', 'UTF-8');
    let d = JSON.parse(data);
    for (let i = 0; i < d.length; i++) {
        users.push(new User(d[i].steamID, d[i].name, d[i].tract, d[i].discordID, d[i].xp, d[i].isWatching, d[i].notificationNumber, d[i].currentGameId, d[i].level));

        let gameList = '{"game_count":0,"games":[]}';
        try {
            let data = fs.readFileSync("games/"+d[i].steamID+".games", "UTF-8");
            if(data.length !== 0) gameList = data;
        } catch (e) {
            fs.writeFileSync("games/"+d[i].steamID+".games", "[]", "UTF-8");
        }
        games[d[i].steamID] = JSON.parse(gameList);
		
		let inventory = '{"item_count":0,"items":[]}';
        try {
            let data = fs.readFileSync("inventories/"+d[i].steamID+".inv", "UTF-8");
            if(data.length !== 0) inventory = data;
        } catch (e) {
            fs.writeFileSync("inventories/"+d[i].steamID+".inv", inventory, "UTF-8");
        }
        inventories[d[i].steamID] = JSON.parse(inventory);
    }
    DATA_LOADED = true;
    setInterval(() => {
        if (DATA_LOADED) {
            fs.writeFileSync('data.dat', JSON.stringify(users), 'UTF-8');
        }
    }, 2500);



    client.on('ready', () => {
        console.log(`Logged in as ${client.user.tag}!`);
        sendMessage = (message) => {
            client.channels.get(CHANNEL_ID).sendMessage(message);
        };
        setInterval(() => {
            for (let i = 0; i < users.length; i++) {
                users[i].getMovie(client);
                users[i].getXP(client);
                users[i].getGames(client);
				users[i].getCurrentPlayingGame(client);
            }
        }, 12000);
        setInterval(() => {
            for (let i = 0; i < users.length; i++) {
                users[i].sendSMS();
            }
        }, 300000);
		/*setInterval(() => {
			let timeout = 60000;
            for (let i = 0; i < users.length; i++) {
				var index = i;
				setTimeout(() => {
					users[index].getCSGOInventory(client);
				}, timeout);
				timeout += 60000;
            }
        }, 180000);*/
    });

    client.on('message', msg => {
        console.log(msg.content);
        if(msg.author.equals(client.user)) return;
        if(!msg.content.startsWith(config.prefix)) return;
        let args = msg.content.substring(config.prefix.length).split(" ");
        switch (args[0].toLowerCase()) {
            case 'meme':
                let key = client.emojis.randomKey();
                msg.channel.sendMessage("<:"+(client.emojis.get(key).name)+":"+key+">");
                break;
            case 'minesweeper':
                if(!fs.existsSync("minesweeper/" + msg.author.id)) {
                    fs.writeFileSync("minesweeper/" + msg.author.id, "[{},{}]", "UTF-8");
                }
                if(!args[1]) {
                    msg.channel.sendMessage(`Command usage: \`${config.prefix}minesweeper COMMAND\`
\`${config.prefix}minesweeper start WIDTH HEIGHT MINE_COUNT\` - creates a game with WIDTH x HEIGHT grid and MINE_COUNT mines
\`${config.prefix}minesweeper open X Y\` - Opens a cell at X:Y coordinate, only when game is started
\`${config.prefix}minesweeper mark X Y\` - Marks a cell at X:Y coordinate as potential mine, only when game is started
\`${config.prefix}minesweeper check X Y\` - Opens a cells around X:Y coordinate if cell number matches flag count around it, only when game is started
\`${config.prefix}minesweeper map\` - Shows minesweeper map if game is started`);
                    break;
                }
                switch (args[1].toLowerCase()) {
                    case 'start':
                        if(args[2] && args[3] && args[4]) {

                            let x = parseInt(args[2]);
                            let y = parseInt(args[3]);
                            let mines = parseInt(args[4]);

                            if(x * y > 800) {
                                msg.channel.sendMessage("Minesweeper area cannot be larger than 800 cells");
                                break;
                            }

                            let map = [];
                            let mapVisible = [];

                            for (let r = 0; r <= y; r++) {
                                let line = [];
                                let lineVisible = [];
                                for (let l = 0; l <= x; l++) {
                                    line.push(0);
                                    lineVisible.push('n');
                                }
                                map.push(line);
                                mapVisible.push(lineVisible);
                            }

                            for (let i = 0; i < mines; i++) {
                                let randY = Math.round(Math.random() * (map.length - 1));
                                let randX = Math.round(Math.random() * (map[randY].length - 1));
                                let coordinate = {x: randX, y: randY};
                                if (map[coordinate.y][coordinate.x] === 0)
                                    map[coordinate.y][coordinate.x] = 'x';
                                else
                                    i--;
                            }

                            for (let y = 0; y < map.length; y++) {
                                for (let x = 0; x < map[y].length; x++) {
                                    if (map[y][x] === 0) {
                                        let mines = 0;
                                        if (x > 0 && y > 0 && map[y - 1][x - 1] === 'x') mines += 1;
                                        if (x < map[y].length - 1 && y > 0 && map[y - 1][x + 1] === 'x') mines += 1;
                                        if (x > 0 && y < map.length - 1 && map[y + 1][x - 1] === 'x') mines += 1;
                                        if (x < map[y].length - 1 && y < map.length - 1 && map[y + 1][x + 1] === 'x') mines += 1;
                                        if (x > 0 && map[y][x - 1] === 'x') mines += 1;
                                        if (x < map[y].length - 1 && map[y][x + 1] === 'x') mines += 1;
                                        if (y > 0 && map[y - 1][x] === 'x') mines += 1;
                                        if (y < map.length - 1 && map[y + 1][x] === 'x') mines += 1;
                                        map[y][x] = mines;
                                    }
                                }
                            }

                            fs.writeFileSync(`minesweeper/${msg.author.id}`, JSON.stringify({map: map, mapVisible: mapVisible}), 'UTF-8');
                            msg.channel.sendMessage("Game started. You can now play!");
                            let generatedMap = "```";
                            for (let y = 0; y < mapVisible.length; y++) {
                                for (let x = 0; x < mapVisible[y].length; x++) {
                                    generatedMap += mapVisible[y][x] + (x < mapVisible[y].length - 1 ? ' ' : '');
                                }
                                if(y < mapVisible.length-1) {
                                    generatedMap += "\n";
                                }
                            }
                            msg.channel.sendMessage(generatedMap+"```");

                        } else {
                            msg.channel.sendMessage(`Invalid command. Use: \`${config.prefix}minesweeper start WIDTH HEIGHT MINE_COUNT\``);
                            break;
                        }
                        break;

                    case 'check':

                        break;

                    case 'open':
                        if(args[2] && args[3]) {
                            let x = parseInt(args[2]);
                            let y = parseInt(args[3]);

                            let userData = fs.readFileSync(`minesweeper/${msg.author.id}`, "UTF-8");
                            let data = JSON.parse(userData.toString());
                            let map = data.map;
                            let mapVisible = data.mapVisible;

                            let newData = openCell(msg, map, mapVisible, x, y);
                            map = newData.map;
                            mapVisible = newData.mapVisible;

                            fs.writeFileSync(`minesweeper/${msg.author.id}`, JSON.stringify(newData), 'UTF-8');
                            let generatedMap = "```";
                            for (let y = 0; y < mapVisible.length; y++) {
                                for (let x = 0; x < mapVisible[y].length; x++) {
                                    generatedMap += mapVisible[y][x] + (x < mapVisible[y].length - 1 ? ' ' : '');
                                }
                                if(y < mapVisible.length-1) {
                                    generatedMap += "\n";
                                }
                            }
                            msg.channel.sendMessage(generatedMap+"```");
                        }
                        break;
                    default:

                        break;
                }
                break;
        }
    });
    process.stdin.resume();
    process.stdin.setEncoding('utf8');
    process.stdin.on('data', function (text) {
        let message = text.trim().split(" ");
        switch (message[0]) {
            case 'send':
                sendMessage(text.substr(5));
                break;
            case 'add':
                users.push(new User(message[1], message[2], message[3], message[4], 0, '', '', '', 0));
                break;
        }
    });
    client.login(config.discord.api);
} catch (e) {
    console.log(e);
}

