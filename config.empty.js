let config = {};

config.steam = {};
config.discord = {};
config.sms = {};

config.preix = '';

config.steam.api = '';
config.discord.api = '';
config.sms.username = '';
config.sms.password = '';
config.sms.sender = '';

module.exports = config;